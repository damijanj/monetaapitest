﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;

namespace EMonetaAPI_test
{
    class MonetaAPITest
    {

        public void ListCertificates()
        {
            X509Store cryptoStore = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            cryptoStore.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection certs = (X509Certificate2Collection)cryptoStore.Certificates.Find(X509FindType.FindByIssuerDistinguishedName, "CN=Telekom Storitve CA1, O=Telekom Slovenije, C=SI", true);
            foreach (X509Certificate2 c in certs)
            {
                Console.WriteLine("Certificate '{1}' issued by '{0}', subject: {2}.", c.Issuer, c.FriendlyName, c.SubjectName.Name);
            }
            cryptoStore.Close();
        }

        /// <summary>
        /// Izvede testni klic na osnovi instaliranega certifikata
        /// </summary>
        public void DoGetToken()
        {
            // HTTPS/SSL transport
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
            // TESTNI SERVIS ZA PRODUKCIJO
            EndpointAddress endpoint = new EndpointAddress("https://eterminal.moneta.si/mpay.mgw.public/public.asmx.");
            MonetaService.PublicSoapClient client = new MonetaService.PublicSoapClient(binding, endpoint);
            client.ClientCredentials.ClientCertificate.SetCertificate(StoreLocation.CurrentUser, StoreName.My, X509FindType.FindBySubjectName, "API MID=001100000310/PID=13815");

            // pin=NULL če varnostna koda ni nastavljena?
            MonetaService.LogInWs login = client.LogIn(null);
            if (login.ErrorCode == 0)
            {
                System.Console.WriteLine("CustomName: {0}, ActiveCashier: {1}", login.CustomName, login.ActiveCashier);
                // pin=NULL če varnostna koda ni nastavljena?
                MonetaService.TokenObject token = client.GetToken(null, "0.99 EUR", "TEST0001", false, 60);
                if (token.ErrorCode == 0)
                {
                    System.Console.WriteLine("TOKEN: {0}", token.Token);
                    // NADALJEVANJE POSTOPKA
                }
                else
                    // KONEC z napako.
                    System.Console.WriteLine("GetToken(): {0} {1}", token.ErrorCode, token.ErrorDescription);
                
            }
            else
                // Prijava ni uspela.
                System.Console.WriteLine("LogIn(): {0} {1}", login.ErrorCode, login.ErrorDescription);
        }

    }


}
